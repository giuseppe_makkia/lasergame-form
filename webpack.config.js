"use strict";

let HtmlWebpackPlugin = require('html-webpack-plugin');
let ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');

module.exports = {
    entry: "./app/app.js",
    output: {
      path: __dirname + "/dist",
      filename: "bundle.js"
    },
    devServer: {
      // Fallback all 404 request to the index
      // https://webpack.github.io/docs/webpack-dev-server.html#the-historyapifallback-option
      historyApiFallback: true
    },
    devtool: "eval",
    module: {
      loaders: [
        { test: /\.css$/, loader: "css-loader" },
        {
          test: /\.scss$/,
          loader: "style!css?sourceMap!sass?sourceMap"
        },
        {
          // ES6 (not only) interpreter
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
          query: {
            presets: ['es2015']
          }
        },
        {
          // HTML LOADER
          // Reference: https://github.com/webpack/raw-loader
          // Allow loading html through js
          test: /\.html$/,
          loader: 'raw'
        },
        {
          // Pre-process images and SVG files, minify and serve them through js
          test: /\.(jpe?g|png|gif|svg)$/i,
          exclude: /node_modules/,
          loaders: [
            'file?name=assets/[name].[ext]',
            'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
          ]
        }
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: "./app/index.html"
      }),
      new ScriptExtHtmlWebpackPlugin({
        defaultAttribute: 'async'
      })
  ]
};
