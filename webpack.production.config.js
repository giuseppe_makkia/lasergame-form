"use strict";

let HtmlWebpackPlugin = require('html-webpack-plugin');
let ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
let webpack = require('webpack');

module.exports = {
    entry: "./app/app.js",
    output: {
      path: __dirname + "/dist",
      filename: "src/[hash].bundle.js",
      chunkFilename: "src/[id].[hash].bundle.js"
    },
    devServer: {
      // Fallback all 404 request to the index
      // https://webpack.github.io/docs/webpack-dev-server.html#the-historyapifallback-option
      historyApiFallback: true
    },
    module: {
      loaders: [
        { test: /\.css$/, loader: "css-loader" },
        {
          test: /\.scss$/,
          loader: "style!css?sourceMap!sass?sourceMap"
        },
        {
          // ES6 (not only) interpreter
          test: /\.js$/,
          loader: 'babel-loader',
          exclude: /node_modules/,
          query: {
            presets: ['es2015']
          }
        },
        {
          // HTML LOADER
          // Reference: https://github.com/webpack/raw-loader
          // Allow loading html through js
          test: /\.html$/,
          loader: 'raw'
        },
        {
          // Pre-process images and SVG files, minify and serve them through js
          test: /\.(jpe?g|png|gif|svg)$/i,
          exclude: /node_modules/,
          loaders: [
            'file?hash=sha512&digest=hex&name=assets/[hash].[ext]',
            'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
          ]
        }
      ]
    },
    plugins: [
      new HtmlWebpackPlugin({
        template: "./app/index.html",
        minify: {
          collapseWhitespace: true,
          removeComments: true,
          removeRedundantAttributes: true,
          removeScriptTypeAttributes: true,
          removeStyleLinkTypeAttributes: true
        },
        cache: true
      }),
      new ScriptExtHtmlWebpackPlugin({
        defaultAttribute: 'async'
      }),
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      }),
      new webpack.optimize.UglifyJsPlugin({
        compressor: {
          warnings: false
        }
      }),
      new webpack.optimize.DedupePlugin()
  ]
};
