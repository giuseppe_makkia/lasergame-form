'use strict';

class HomeController {
  constructor() {
    console.log('inside home controller')
    this.name = 'World!';
  }
}

export default angular
  .module('app.home.controller', [])
  .controller('HomeController', HomeController);
