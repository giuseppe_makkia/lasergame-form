'use strict';

import angular from 'angular'

homeRouting.$inject = ['$stateProvider'];

function homeRouting($stateProvider) {

  $stateProvider
    .state('root.home', {
      url: '/',
      // resolve: {
      //   loadHomeController: ($q, $ocLazyLoad) => {
      //     // let module = require('./home.controller');
      //     // console.log($ocLazyLoad.isLoaded('app.home.controller'))
      //     // return $ocLazyLoad.load({name: module.name});
      //     return $q((resolve) => {
      //       require.ensure([], () => {
      //         // load only controller module
      //         let module = require('./home.controller');
      //         $ocLazyLoad.load({name: module.name});
      //         resolve(module.controller);
      //       })
      //     });
      //   }
      // },
      // controller: 'HomeController as home',
      views: {
        '@': {
          // template: require('./home.html'),
          templateProvider: ['$q', ($q) => {
            return $q((resolve) => {
              // lazy load the view
              require.ensure([], () => resolve(require('./home.html')));
            });
          }],
          // resolve: {
          //   loadHomeController: ($q, $ocLazyLoad) => {
          //     return $q((resolve) => {
          //       require.ensure([], () => {
          //         // load only controller module
          //         let module = require('./home.controller');
          //         $ocLazyLoad.load({name: module.name});
          //         resolve(module.controller);
          //       })
          //     });
          //   }
          // },
          controller: 'HomeController',
          controllerAs: 'home',
          resolve: {
            load: ['$q', '$ocLazyLoad', function($q, $ocLazyLoad) {
              return $q((resolve) => {
                require.ensure([], () => {
                  let module = require('./home.controller');
                  // lazy load the view
                  $ocLazyLoad.load({
                    name: module.default.name
                  });
                  resolve(module.controller)
                });
              });
            }]
          }
        }
      }
    });
}

export default angular
  .module('app.home.routing', [])
  .config(homeRouting)
  .name;
