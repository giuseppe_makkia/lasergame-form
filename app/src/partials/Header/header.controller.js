'use strict';

class HeaderController {
  constructor() {
    console.log('inside header controller')
    this.name = 'Header';
  }
}

export default angular
  .module('app.header.controller', [])
  .controller('HeaderController', HeaderController);
