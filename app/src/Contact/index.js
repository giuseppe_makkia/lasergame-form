import './contact.scss';

import angular from 'angular';
// import uirouter from 'angular-ui-router';

// import routing from './contact.routes';
import ContactController from './contact.controller';

export default angular.module('app.contact', [])
  // .config(routing)
  .controller('ContactController', ContactController)
  .name;
