'use strict';

import angular from 'angular'

class ContactController {
  constructor() {
    console.log('inside contact controller')
    this.name = 'Contact!';
    this.imageUrl = require('../../assets/fleka-logo.png');
  }
}

export default angular
  .module('app.contact.controller', [])
  .controller('ContactController', ContactController);
