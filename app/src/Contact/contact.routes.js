'use strict';

import angular from 'angular'

contactRouting.$inject = ['$stateProvider'];

function contactRouting($stateProvider) {

  $stateProvider
    .state('root.contact', {
      url: '/contact',
      // resolve: {
      //   loadHomeController: ($q, $ocLazyLoad) => {
      //     // let module = require('./home.controller');
      //     // console.log($ocLazyLoad.isLoaded('app.home.controller'))
      //     // return $ocLazyLoad.load({name: module.name});
      //     return $q((resolve) => {
      //       require.ensure([], () => {
      //         // load only controller module
      //         let module = require('./home.controller');
      //         $ocLazyLoad.load({name: module.name});
      //         resolve(module.controller);
      //       })
      //     });
      //   }
      // },
      // controller: 'HomeController as home',
      views: {
        '@': {
          templateProvider: ['$q', ($q) => {
            return $q((resolve) => {
              // lazy load the view
              require.ensure([], () => resolve(require('./contact.html')));
            });
          }],
          controller: 'ContactController as contact',
          resolve: {
            load: ['$q', '$ocLazyLoad', function($q, $ocLazyLoad) {
              return $q((resolve) => {
                require.ensure([], () => {
                  let module = require('./contact.controller');
                  // lazy load the view
                  $ocLazyLoad.load({
                    name: module.default.name
                  });
                  resolve(module.controller)
                });
              });
            }]
          }
        }
      }
    });
}

export default angular
  .module('app.contact.routing', [])
  .config(contactRouting)
  .name;
