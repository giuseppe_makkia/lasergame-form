import 'normalize.css'
import './style.scss'

import angular from 'angular'
import uirouter from 'angular-ui-router'

import routing from './app.config'
import homeRoutes from './src/Home/home.routes'
import contactRoutes from './src/Contact/contact.routes'

angular
  .module('app', [
    uirouter,
    require('oclazyload'),
    homeRoutes,
    contactRoutes,
  ])
  .config(routing);
