routing.$inject = ['$urlRouterProvider', '$locationProvider', '$stateProvider'];

export default function routing($urlRouterProvider, $locationProvider, $stateProvider) {
  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/');

  $stateProvider
  .state('root', {
    abstract: true,
    views: {
      'header': {
        templateProvider: ['$q', ($q) => {
          return $q((resolve) => {
            // lazy load the view
            require.ensure([], () => resolve(require('./src/partials/Header/header.html')));
          });
        }],
        controller: 'HeaderController as header',
        resolve: {
          load: ['$q', '$ocLazyLoad', function($q, $ocLazyLoad) {
            return $q((resolve) => {
              require.ensure([], () => {
                let module = require('./src/partials/Header/header.controller');
                // lazy load the view
                $ocLazyLoad.load({
                  name: module.default.name
                });
                resolve(module.controller)
              });
            });
          }]
        }
      }
    }
  });
}
