# Webpack Base Example Project
This is a simple repository to pack the basic configurations and node_modules needed to run overall our webpack projects.

# CLI shortcuts
`npm start`
Runs the webpack-dev-server with --inline mode configured. --inline is not going to create files physicall on the hard disk, but everything will run from the memory.

`npm run build`
This packs the "dist/" folder, ready for production.

`npm run dist`
If you don't want to use your Apache local configuration and you don't want to set your local hosts file, you can run this very simple and basic python server to preview your dist folder before uploading it to production.

**TODO** this still doesn't support any request to be redirected to index.html, so you always have to load the / state for now.
